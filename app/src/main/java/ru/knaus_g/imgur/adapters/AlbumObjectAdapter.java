package ru.knaus_g.imgur.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.knaus_g.imgur.R;
import ru.knaus_g.imgur.entities.AlbumObject;

public class AlbumObjectAdapter extends RecyclerView.Adapter<AlbumObjectAdapter.ViewHolder> {

    @NonNull
    private List<AlbumObject> arrayAlbum = new ArrayList<>();
    @Nullable
    private AdapterListener adapterListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_fragment_main_adapter, parent, false));

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterListener != null) {
                    adapterListener.onItemClick(arrayAlbum.get(vh.getAdapterPosition()));
                }
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(arrayAlbum.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayAlbum.size();
    }

    public void addAll(List<AlbumObject> list){
        arrayAlbum.clear();
        arrayAlbum.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageAlbum;
        private TextView textAlbumName;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAlbum = itemView.findViewById(R.id.imageAlbum);
            textAlbumName = itemView.findViewById(R.id.textAlbumName);
        }

        void bind(AlbumObject albumObject){
            Glide
                    .with(itemView)
                    .load(albumObject.getImage())
                    .into(imageAlbum);
            textAlbumName.setText(albumObject.getName());

        }
    }

    public void setAdapterListener(@NonNull AdapterListener adapterListener){
        this.adapterListener = adapterListener;
    }


    public interface AdapterListener{
        void onItemClick(AlbumObject albumObject);
    }
}

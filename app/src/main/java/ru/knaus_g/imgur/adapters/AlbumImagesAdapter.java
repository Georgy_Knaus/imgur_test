package ru.knaus_g.imgur.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.knaus_g.imgur.R;

public class AlbumImagesAdapter extends RecyclerView.Adapter<AlbumImagesAdapter.ViewHolder> {

    @NonNull
    private List<String> arrayImage = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_fragment_additional_images, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(arrayImage.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayImage.size();
    }

    public void addAll(List<String> list){
        arrayImage.clear();
        arrayImage.addAll(list);
        notifyDataSetChanged();
    }


    public void clear(){
        arrayImage.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imagesAlbum;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagesAlbum = itemView.findViewById(R.id.imagesAlbum);
        }

        void bind(String str){
            Glide
                    .with(itemView)
                    .load(str)
                    .into(imagesAlbum);

        }
    }
}

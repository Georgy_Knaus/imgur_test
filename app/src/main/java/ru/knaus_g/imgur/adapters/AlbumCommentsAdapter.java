package ru.knaus_g.imgur.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.knaus_g.imgur.R;
import ru.knaus_g.imgur.entities.AlbumCommentsObject;

public class AlbumCommentsAdapter extends RecyclerView.Adapter<AlbumCommentsAdapter.ViewHolder> {


    @NonNull
    private List<AlbumCommentsObject> arrayComments = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fragment_additional_comments, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(arrayComments.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayComments.size();
    }

    public void addAll(List<AlbumCommentsObject> list){
        arrayComments.clear();
        arrayComments.addAll(list);
        notifyDataSetChanged();
    }

    public void clear(){
        arrayComments.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textAuthorAndDate;
        private TextView textComment;
        private ImageView imageComment;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textAuthorAndDate = itemView.findViewById(R.id.textAuthorAndDate);
            textComment = itemView.findViewById(R.id.textComment);
            imageComment = itemView.findViewById(R.id.imageComment);
        }

        @SuppressLint("SetTextI18n")
        void bind(AlbumCommentsObject albumCommentsObject){
            textAuthorAndDate.setText(albumCommentsObject.getAuthor() + " " + albumCommentsObject.getTime());


            if(albumCommentsObject.getComment().contains(".gif")){
                String image = albumCommentsObject.getComment().substring(albumCommentsObject.getComment().indexOf("http"), albumCommentsObject.getComment().lastIndexOf(".gif")) + ".gif";

                textComment.setText(albumCommentsObject.getComment().replace(image, ""));
                imageComment.setVisibility(View.VISIBLE);

                Glide
                        .with(itemView)
                        .load(image)
                        .into(imageComment);
                imageComment.setMaxHeight(5);
                imageComment.setMaxWidth(5);
            } else{ if(albumCommentsObject.getComment().contains(".png")){
                String image = albumCommentsObject.getComment().substring(albumCommentsObject.getComment().indexOf("http"), albumCommentsObject.getComment().lastIndexOf(".png")) + ".png";

                textComment.setText(albumCommentsObject.getComment().replace(image, ""));
                imageComment.setVisibility(View.VISIBLE);
                imageComment.setMaxHeight(5);
                imageComment.setMaxWidth(5);
                Glide
                        .with(itemView)
                        .load(image)
                        .into(imageComment);
            } else{
                if(albumCommentsObject.getComment().contains(".jpg")){
                    String image = albumCommentsObject.getComment().substring(albumCommentsObject.getComment().indexOf("http"), albumCommentsObject.getComment().lastIndexOf(".jpg")) + ".jpg";

                    textComment.setText(albumCommentsObject.getComment().replace(image, ""));
                    imageComment.setVisibility(View.VISIBLE);
                    imageComment.setMaxHeight(5);
                    imageComment.setMaxWidth(5);
                    Glide
                            .with(itemView)
                            .load(image)
                            .into(imageComment);
                } else{
                    imageComment.setVisibility(View.GONE);
                    imageComment.setMaxHeight(0);
                    imageComment.setMaxWidth(0);
                    textComment.setText(albumCommentsObject.getComment());
                }
            }
            }
        }
    }
}

package ru.knaus_g.imgur.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;
import java.util.Objects;

import ru.knaus_g.imgur.R;
import ru.knaus_g.imgur.adapters.AlbumObjectAdapter;
import ru.knaus_g.imgur.entities.AlbumObject;
import ru.knaus_g.imgur.vm.FragmentMainModel;

public class FragmentMain extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @NonNull
    private AlbumObjectAdapter adapter = new AlbumObjectAdapter();
    private FragmentMainModel model;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FragmentAdditionalInfo fragmentAdditionalInfo = new FragmentAdditionalInfo();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_main, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewAlbum);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else{
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        }
         recyclerView.setAdapter(adapter);


        adapter.setAdapterListener(new AlbumObjectAdapter.AdapterListener() {
            @Override
            public void onItemClick(AlbumObject albumObject) {

                Bundle bundle = new Bundle();
                bundle.putString("name", albumObject.getName());
                bundle.putString("id", albumObject.getId());
                fragmentAdditionalInfo.setArguments(bundle);

                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction().replace(R.id.frameLayout, fragmentAdditionalInfo, null).addToBackStack(null).commit();
                }
            }
        });


        swipeRefreshLayout = view.findViewById(R.id.swipeLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);



        model = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                //noinspection unchecked
                return (T) new FragmentMainModel(Objects.requireNonNull(getActivity()).getApplication(), swipeRefreshLayout);
            }
        }).get(FragmentMainModel.class);

        model.getLivaData().observe(this, new Observer<List<AlbumObject>>() {
            @Override
            public void onChanged(List<AlbumObject> list) {
                adapter.addAll(list);
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        // Получение Альбомов
        model.getAlbums();

        return view;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        model.getAlbums();
    }
}

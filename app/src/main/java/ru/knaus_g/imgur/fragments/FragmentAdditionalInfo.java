package ru.knaus_g.imgur.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;
import java.util.Objects;

import ru.knaus_g.imgur.R;
import ru.knaus_g.imgur.adapters.AlbumCommentsAdapter;
import ru.knaus_g.imgur.adapters.AlbumImagesAdapter;
import ru.knaus_g.imgur.entities.AlbumCommentsObject;
import ru.knaus_g.imgur.vm.FragmentAdditionalInfoModel;

public class FragmentAdditionalInfo extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Nullable
    private FragmentAdditionalInfoModel model;
    private AlbumImagesAdapter albumImagesAdapter = new AlbumImagesAdapter();
    private AlbumCommentsAdapter albumCommentsAdapter = new AlbumCommentsAdapter();
    private String id;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view =
                inflater.inflate(R.layout.fragment_additional_info, container, false);

        TextView textView = view.findViewById(R.id.textName);
        if (getArguments() != null) {
            textView.setText(getArguments().getString("name"));
        }

        assert getArguments() != null;
        id = getArguments().getString("id");

        swipeRefreshLayout = view.findViewById(R.id.swipeFragmentAdditional);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        RecyclerView recyclerViewImage = view.findViewById(R.id.recyclerAlbumImage);
        recyclerViewImage.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerViewImage.setAdapter(albumImagesAdapter);


        RecyclerView recyclerViewComment = view.findViewById(R.id.recyclerComments);
        recyclerViewComment.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewComment.setAdapter(albumCommentsAdapter);


        model = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                //noinspection unchecked
                return (T) new FragmentAdditionalInfoModel(Objects.requireNonNull(getActivity()).getApplication(), swipeRefreshLayout);
            }
        }).get(FragmentAdditionalInfoModel.class);


        model.getImageLivaData().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> list) {
                albumImagesAdapter.addAll(list);
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        model.getCommentsLivaData().observe(this, new Observer<List<AlbumCommentsObject>>() {
            @Override
            public void onChanged(List<AlbumCommentsObject> list) {
                albumCommentsAdapter.addAll(list);
            }
        });

        model.getAlbumImages(id);
        model.getAlbumComments(id);

        return view;
    }

    @Override
    public void onRefresh() {
        if (model != null) {
            model.getAlbumImages(id);
            model.getAlbumComments(id);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        albumImagesAdapter.clear();
        albumCommentsAdapter.clear();
    }
}

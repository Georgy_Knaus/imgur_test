package ru.knaus_g.imgur.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import ru.knaus_g.imgur.fragments.FragmentMain;
import ru.knaus_g.imgur.R;

public class MainActivity extends AppCompatActivity {

    @NonNull
    private FragmentMain fragmentMain = new FragmentMain();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragmentMain, null).addToBackStack(null).commit();
            }


            // ActionBar
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
            }
        }

        @Override
        public boolean onOptionsItemSelected (@NonNull MenuItem item){
            int id = item.getItemId();

            if (id == android.R.id.home && getSupportFragmentManager().getBackStackEntryCount() > 1) {
                super.onBackPressed();
                return true;
            } else {
                onBackPressed();
            }
            return false;
        }

        @Override
        public void onBackPressed () {
            finish();
        }
    }


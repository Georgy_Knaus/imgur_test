package ru.knaus_g.imgur.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.knaus_g.imgur.entities.AlbumCommentsObject;

public class FragmentAdditionalInfoModel extends AndroidViewModel {

    private MutableLiveData<List<String>> liveDataImage = new MutableLiveData<>();
    private MutableLiveData<List<AlbumCommentsObject>> liveDataComments = new MutableLiveData<>();
    private SwipeRefreshLayout swipeRefreshLayout;

    public FragmentAdditionalInfoModel(@NonNull Application application, SwipeRefreshLayout swipeRefreshLayout) {
        super(application);
        this.swipeRefreshLayout = swipeRefreshLayout;
    }


    public MutableLiveData<List<String>> getImageLivaData(){
        return liveDataImage;
    }


    public MutableLiveData<List<AlbumCommentsObject>> getCommentsLivaData(){
        return liveDataComments;
    }


    public void getAlbumImages(String id){

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://api.imgur.com/3/album/" + id +  "/images")
                .method("GET", null)
                .addHeader("Authorization", "Client-ID 25a622fafbf1f0d")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                List<String> arrayAlbumImages = new ArrayList<>();
                JSONObject object;
                try {
                    assert response.body() != null;
                    object = new JSONObject(response.body().string());
                    JSONArray array = object.getJSONArray("data");

                    for(int i =0; i<array.length(); i++){
                        JSONObject arrayObject = array.getJSONObject(i);
                        String image = arrayObject.getString("link"); // Получение картинки альбома

                        arrayAlbumImages.add(image);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                liveDataImage.postValue(arrayAlbumImages);
            }
        });
    }


    public void getAlbumComments(String id){

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://api.imgur.com/3/gallery/" + id +  "/comments/new")
                .method("GET", null)
                .addHeader("Authorization", "Client-ID 25a622fafbf1f0d")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                List<AlbumCommentsObject> arrayAlbumComments = new ArrayList<>();
                JSONObject object;
                try {
                    assert response.body() != null;
                    object = new JSONObject(response.body().string());
                    JSONArray array = object.getJSONArray("data");

                    for(int i =0; i<array.length(); i++){
                        JSONObject arrayObject = array.getJSONObject(i);
                        String author = arrayObject.getString("author"); // Получение Автора
                        String date = arrayObject.getString("datetime"); // Получение Даты
                        String comment = arrayObject.getString("comment"); // Получение Комментария

                        arrayAlbumComments.add(new AlbumCommentsObject(date, author, comment));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                liveDataComments.postValue(arrayAlbumComments);
            }
        });
    }
}

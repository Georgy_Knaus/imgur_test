package ru.knaus_g.imgur.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.knaus_g.imgur.entities.AlbumObject;

public class FragmentMainModel extends AndroidViewModel {

    private MutableLiveData<List<AlbumObject>> liveData = new MutableLiveData<>();
    private static final String URL = "https://api.imgur.com";
    private SwipeRefreshLayout swipeRefreshLayout;

    public FragmentMainModel(@NonNull Application application, SwipeRefreshLayout swipeRefreshLayout) {
        super(application);
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    public MutableLiveData<List<AlbumObject>> getLivaData(){
        return liveData;
    }


    public void getAlbums(){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://api.imgur.com/3/gallery/hot/varal/day/1?showViral=true&mature=false&album_previews=true")
                .method("GET", null)
                .addHeader("Authorization", "Client-ID 25a622fafbf1f0d")
                .build();
           client.newCall(request).enqueue(new Callback() {
               @Override
               public void onFailure(@NonNull Call call, @NonNull IOException e) {
                   swipeRefreshLayout.setRefreshing(false);
               }

               @Override
               public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                   List<AlbumObject> arrayAlbum = new ArrayList<>();

                   JSONObject object;
                   try {
                       assert response.body() != null;
                       object = new JSONObject(response.body().string());

                       for(int i =0; i<40; i++){
                           JSONArray array = object.getJSONArray("data");
                           JSONObject title = array.getJSONObject(i);
                           if(title.getBoolean("is_album")){    // Проверка на Альбом
                               String id = title.getString("id");      //Получение id Альбома

                               String name = title.getString("title");      //Получение названия Альбома

                               JSONArray arrayone = title.getJSONArray("images");
                               JSONObject peremennaya = arrayone.getJSONObject(0);
                               String picture = peremennaya.getString("link"); // Получение картинки альбома

                               arrayAlbum.add(new AlbumObject(id, name, picture));
                           }
                       }

                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                       liveData.postValue(arrayAlbum);

               }
           });
    }

}

package ru.knaus_g.imgur.entities;

import android.annotation.SuppressLint;

import androidx.annotation.Nullable;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class AlbumCommentsObject {

    private String time;
    private String author;
    private String comment;

    public AlbumCommentsObject(String time, String author, String comment) {
        this.time = time;
        this.author = author;
        this.comment = comment;
    }

    public String getTime() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatForecast = new SimpleDateFormat("dd-MM-yy kk:mm");
        long newTime = Long.parseLong(time + "000");
        return dateFormatForecast.format(new Date(newTime));
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, author, comment);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AlbumCommentsObject that = (AlbumCommentsObject) obj;
        return time.equals(that.time) &&
                author.equals(that.author) &&
                comment.equals(that.comment);
    }
}

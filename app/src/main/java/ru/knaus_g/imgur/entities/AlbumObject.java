package ru.knaus_g.imgur.entities;

import androidx.annotation.Nullable;

import java.util.Objects;

public class AlbumObject {

    public AlbumObject(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }


    private String id;
    private String name;
    private String image;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, image);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AlbumObject that = (AlbumObject) obj;
        return name.equals(that.name) &&
                image.equals(that.image);
    }


}
